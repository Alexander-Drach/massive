var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0], i, imax;

var max, min, maxName;

console.log("1. Список студентов:");
  for (i = 0, imax = studentsAndPoints.length / 2; i < imax; i++) {
      console.log("студент - %s набрал - %d баллов", studentsAndPoints[2 * i], studentsAndPoints[2 * i + 1]);
}
  console.log(" ");
console.log("2. Студент набравший максимальный балл:");
  /*for (i = 0, imax = studentsAndPoints.length; i < imax; i++)*/
for (i = 0, imax = studentsAndPoints.length / 2; i < imax; i++) {
  if (max === undefined || studentsAndPoints[2 * i + 1] > max) {
      max = studentsAndPoints[2 * i + 1];
      maxName = studentsAndPoints[2 * i];  
  }
}
console.log("студент - %s набрал - %d баллов", maxName, max);


  console.log(" ");
console.log("3. Список с новыми студентами:");

studentsAndPoints.push("Николай Фролов", 0, "Олег Боровой", 0);

for (i = 0, imax = studentsAndPoints.length / 2; i < imax; i++) {
    console.log("студент - " + studentsAndPoints[2 * i] + " набрал - " + studentsAndPoints[2 * i + 1] + " баллов");
}
  console.log(" "); 
console.log("4. Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов:");
  
var namePlus1, namePlus2, ballPlus, newBall1, newBall2;

var namePlus1 = studentsAndPoints.indexOf('Антон Павлович');
if (namePlus1 != -1) {
ballPlus = studentsAndPoints[namePlus1+1];
newBall1 = ballPlus + 10;
studentsAndPoints[namePlus1+1] = newBall1;

console.log(studentsAndPoints[namePlus1] + " " + studentsAndPoints[namePlus1+1]);
}
var namePlus2 = studentsAndPoints.indexOf('Николай Фролов');
if (namePlus2 != -1) {
ballPlus = studentsAndPoints[namePlus2+1];
newBall2 = ballPlus + 10;
studentsAndPoints[namePlus2+1] = newBall2;

console.log(studentsAndPoints[namePlus2] + " " + newBall2);
}
  console.log(" ");

console.log("5. Студенты, не набравшие баллов:");
  for (i = 0, imax = studentsAndPoints.length / 2; i < imax; i++) {
  if (studentsAndPoints[2 * i + 1] === 0) {
  min = studentsAndPoints[2 * i + 1];
  minName = studentsAndPoints[2 * i];
  console.log("студент - %s набрал - %d баллов", minName, min);
  }
}
